#![warn(clippy::nursery, clippy::pedantic)]
#![deny(unsafe_code)]

mod constant;
mod program;
mod token;

use program::Program;
use std::fs::File;
use std::io::{stdin, stdout, Read, Result, Write};
use std::path::{Path, PathBuf};
use structopt::StructOpt;
use token::Stream;

fn main() {
    let arguments = Arguments::from_args();

    let source = read_file(arguments.input)
        .unwrap_or_else(|err| die(&format!("Could not read the source: {}", err)));

    let stream = Stream::new(&source);
    match Program::from_stream(stream) {
        Ok(mut program) => {
            program.expand_labels();
            let assembled = program.assembled();
            let assembled = wrap_result(assembled, arguments.wrap);
            write_file(arguments.output, &assembled)
                .unwrap_or_else(|err| die(&format!("Could not write the output: {}", err)))
        }
        Err(error) => {
            error.display(&source);
        }
    }
}

#[derive(StructOpt)]
struct Arguments {
    /// Input file, stdin if not present
    #[structopt(parse(from_os_str))]
    input: Option<PathBuf>,
    /// Output file, stdout if not present
    #[structopt(short, long, parse(from_os_str))]
    output: Option<PathBuf>,
    /// Width of lines in the output. 0 disables wrapping.
    #[structopt(long, default_value = "80")]
    wrap: usize,
}

fn read_file(path: Option<impl AsRef<Path>>) -> Result<String> {
    if let Some(path) = path {
        std::fs::read_to_string(path)
    } else {
        let mut buf = String::new();
        stdin().lock().read_to_string(&mut buf)?;
        Ok(buf)
    }
}

fn write_file(path: Option<impl AsRef<Path>>, data: &[u8]) -> Result<()> {
    if let Some(path) = path {
        File::create(path)?.write_all(data)
    } else {
        stdout().lock().write_all(data)
    }
}

fn die(message: &str) -> ! {
    eprintln!("\x1B[1;31merror:\x1B[0m {}", message);
    std::process::exit(1);
}

fn wrap_result(mut bytes: Vec<u8>, width: usize) -> Vec<u8> {
    if width == 0 {
        if !bytes.is_empty() {
            bytes.push(b'\n');
        }
        bytes
    } else {
        let mut result = Vec::new();
        for chunk in bytes.chunks(width) {
            result.extend_from_slice(chunk);
            result.push(b'\n');
        }
        result
    }
}
