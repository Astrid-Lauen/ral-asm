use std::collections::HashMap;
use std::rc::Rc;

pub struct Cache {
    data: HashMap<usize, Rc<str>>,
}

impl Cache {
    pub fn new() -> Self {
        Self {
            data: HashMap::new(),
        }
    }
    pub fn get(&mut self, n: usize) -> Rc<str> {
        if let Some(code) = self.data.get(&n) {
            code.clone()
        } else {
            let code: Rc<str> = match n {
                n if n < 2 => n.to_string().into(),
                n if n % 2 == 0 => format!("{}:+", self.get(n / 2)).into(),
                n => {
                    let sp = self.get(n - 1);
                    let sm = self.get(n + 1);
                    if sp.len() <= sm.len() {
                        format!("1{}+", sp).into()
                    } else {
                        format!("1{}-", sm).into()
                    }
                }
            };
            self.data.insert(n, code.clone());
            code
        }
    }
}
