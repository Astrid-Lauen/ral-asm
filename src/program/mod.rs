mod error;
pub use error::ParseError;

use crate::constant::Cache;
use crate::token::{Stream, Token};
use std::collections::HashMap;
use std::mem::replace;

#[derive(Debug)]
pub struct Program {
    code: Vec<Code>,
}

#[derive(Debug)]
pub enum Code {
    Block(Vec<u8>),
    Label { dest: usize, size: usize },
}

macro_rules! error {
    ($span:expr, $($args:expr),*) => {
        return Err(ParseError::new(format!($($args),*), $span))
    };
}

impl Program {
    pub fn from_stream(mut stream: Stream) -> Result<Self, ParseError> {
        let mut refs = Vec::new();
        let mut labels = HashMap::new();
        let mut code = Vec::new();
        let mut block = None;
        loop {
            let (span, token) = stream.next();

            match token {
                Token::Opcode(opcode) => {
                    block.get_or_insert_with(Vec::new).push(opcode);
                }
                Token::Label(label) => {
                    code.extend(block.take().map(Code::Block));
                    refs.push((code.len(), label, span));
                    code.push(Code::Label { dest: 0, size: 0 });
                }
                Token::StartDefine => {
                    code.extend(block.take().map(Code::Block));
                    let label = match stream.next() {
                        (_, Token::Label(label)) => label,
                        (span, token) => error!(span, "Expected label, found {}", token),
                    };
                    match stream.next() {
                        (_, Token::EndDefine) => {}
                        (span, token) => error!(span, "Expected ']', found {}", token),
                    }
                    labels.insert(label, code.len());
                }
                Token::End => {
                    code.extend(block.take().map(Code::Block));
                    break;
                }
                token => error!(span, "Unexpected {}", token),
            }
        }

        for (i, label, span) in refs {
            if let Some(&dest) = labels.get(label) {
                code[i] = Code::Label { dest, size: 0 };
            } else {
                error!(span, "The label {} is not defined", label);
            }
        }

        Ok(Self { code })
    }
    fn indexes(&self) -> impl Iterator<Item = usize> + '_ {
        Some(0)
            .into_iter()
            .chain(self.code.iter().scan(0, |index, code| {
                *index += match *code {
                    Code::Block(ref block) => block.len(),
                    Code::Label { size, .. } => size,
                };

                Some(*index)
            }))
    }
    pub fn expand_labels(&mut self) {
        let mut any_changed = true;
        let mut cache = Cache::new();
        while replace(&mut any_changed, false) {
            let lengths: Vec<usize> = self.indexes().map(|i| cache.get(i).len()).collect();

            for code in &mut self.code {
                if let Code::Label { dest, ref mut size } = *code {
                    if *size < lengths[dest] {
                        *size = lengths[dest];
                        any_changed = true;
                    }
                }
            }
        }
    }
    pub fn assembled(&self) -> Vec<u8> {
        let indexes: Vec<usize> = self.indexes().collect();
        let mut result = Vec::new();
        let mut cache = Cache::new();
        for code in &self.code {
            match *code {
                Code::Block(ref block) => result.extend_from_slice(block),
                Code::Label { dest, size } => {
                    let code = cache.get(indexes[dest]);
                    assert!(code.len() <= size);
                    result.extend_from_slice(format!("{:_<1$}", code, size).as_bytes())
                }
            }
        }
        result
    }
}
