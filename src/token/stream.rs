use super::{Span, Token};
use unicode_xid::UnicodeXID;

/// A token stream.
pub struct Stream<'a> {
    source: &'a str,
    offset: usize,
}

impl<'a> Stream<'a> {
    /// Creates a new token stream.
    pub const fn new(source: &str) -> Stream {
        Stream { source, offset: 0 }
    }
    /// Advances `self` by `len` bytes (not characters).
    fn consume_len(&mut self, len: usize) -> (Span, &'a str) {
        let span = Span(self.offset, self.offset + len);
        let (consumed, rest) = self.source.split_at(len);
        self.source = rest;
        self.offset += len;
        (span, consumed)
    }
    /// Advances `self` while `p` if true.
    fn consume_while(&mut self, mut p: impl FnMut(char) -> bool) -> (Span, &'a str) {
        let len = self
            .source
            .char_indices()
            .find(|&(_, c)| !p(c))
            .map_or(self.source.len(), |(i, _)| i);

        self.consume_len(len)
    }
    /// Gets the next char and its length without consuming it.
    fn next_char(&self) -> Option<(char, usize)> {
        let mut chars = self.source.chars();
        let chr = chars.next()?;
        let len = self.source.len() - chars.as_str().len();
        Some((chr, len))
    }
    /// Parses a single `Token` from `self`, returning it and its `Span`. Returns `Token::End` if
    /// the end of file is reached.
    pub fn next(&mut self) -> (Span, Token<'a>) {
        loop {
            self.consume_while(char::is_whitespace);

            return match self.next_char() {
                Some(('#', _)) => {
                    self.consume_while(|c| c != '\n');
                    continue;
                }
                Some((chr, _)) if chr.is_xid_start() => {
                    let (span, label) = self.consume_while(UnicodeXID::is_xid_continue);
                    (span, Token::Label(label))
                }
                Some((chr, len)) => {
                    let (span, _) = self.consume_len(len);
                    let token = match chr {
                        chr if is_opcode(chr) => Token::Opcode(chr as u8),
                        '[' => Token::StartDefine,
                        ']' => Token::EndDefine,
                        chr => Token::Unknown(chr),
                    };
                    (span, token)
                }
                None => (Span(self.offset, self.offset), Token::End),
            };
        }
    }
}

/// Tests if the character is a Ral opcode.
fn is_opcode(chr: char) -> bool {
    match chr {
        '*' | '+' | ',' | '-' | '.' | '/' | '0' | '1' | ':' | '=' | '?' | '_' => true,
        _ => false,
    }
}
