use std::fmt::{self, Debug, Formatter};

/// The span (in bytes) of a token in the source code.
#[derive(Copy, Clone, Eq, PartialEq, Hash)]
pub struct Span(pub usize, pub usize);

impl Debug for Span {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        write!(f, "{}..{}", self.0, self.1)
    }
}
